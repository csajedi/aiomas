``aiomas.channel``
==================

.. automodule:: aiomas.channel
   :members:
   :exclude-members: DEFAULT_CODEC

   .. attribute:: DEFAULT_CODEC

      Default codec: :class:`~aiomas.codecs.JSON`
