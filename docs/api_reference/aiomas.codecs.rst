``aiomas.codecs``
=================

.. automodule:: aiomas.codecs
   :members:
   :exclude-members: serializable

   .. autofunction:: serializable(repr=True)
