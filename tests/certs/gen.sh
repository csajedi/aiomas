#!/bin/bash
#
# CA password is: aiomas
# CA common name: aiomas-ca
# Device common name: 127.0.0.1

openssl genrsa -aes256 -out ca.key 4096
openssl req -new -x509 -nodes -key ca.key -out ca.pem -days 3650

openssl genrsa -out device.key 4096
openssl req -new -key device.key -out device.csr
openssl x509 -CA ca.pem -CAkey ca.key -CAcreateserial -req -in device.csr -out device.pem -days 3650
